
Module: Correct.li
Author: Pawe� Klaus <http://drupal.org/user/1088412>


Description
===========
Provides integration with Correct.li site.

Requirements
============

* Correct.li user account


Installation
============
* Copy the 'correctli' module directory in to your Drupal
sites/all/modules directory as usual.



What is Correct.li?
====================================================
Correct.li is online web service which lets users correct your mistakes - misspelled words, wrong information, bad grammar - in site's content.
Now your users can correct your mistakes by selecting text on your website. You can see all mistakes in your control panel.
More information at http://correct.li/.

Easy Integration
=================
To integrate Correct.li and your site, you just have to register at http://correct.li/, get your API key and set it in Drupal control panel.

Usage
=================
To correct mistakes users need to select some text, fill the shown form with and press 'enter'. The information is passed via ajax call to corret.li where you can mannage messages.

Why module?
=================
Drupal correct.li module provides (almost) full control over Correct.li script. You can select roles and pages where Correct.li functionality is visible.